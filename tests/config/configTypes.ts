import fs from 'fs';
import toml from 'toml';
const config = toml.parse(fs.readFileSync('./tests/config/config.toml', 'utf-8'));

export default {
  BASEURL: config.base_url ?? '',
  };