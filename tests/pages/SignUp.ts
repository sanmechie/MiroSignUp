import { Page } from '@playwright/test';
import Base from './Base';
import { SIGNUP_CONTINUE_BUTTON } from '../utilities/Constants';

export default class SignUp extends Base {
  /**
   *
   * Sign Up Captures the identifiers and methods required to
   * interact with Sign up Functionaltiy
   *
   */

  readonly id_Title: string;
  readonly id_SubTitle: string;
  readonly id_PlaceHolder: string;
  readonly id_CntBtnLbl: string;
  readonly id_CntBtnEmail: string;
  readonly id_CntBtnName: string;
  readonly id_CntBtnPwd: string;
  readonly id_EnterEmail: string;
  readonly id_EnterName: string;
  readonly id_EnterPwd: string;
  readonly id_CheckBox: string;
  readonly id_CnfTitle: string;
  readonly id_CnfSubTitle: string;
  readonly id_CnfPlaceholderCode: string;
  readonly id_CnfCode: string;
  readonly id_CnfFooter: string;
  readonly id_CnfSignUpSndCodeLnk: string;
  readonly id_CnfSignUpHelpLnk: string;
  readonly id_EmailError: string;
  readonly id_invalidEmailError: string;
  readonly id_nameError: string;
  readonly id_pwdError: string;
  readonly id_termsAndPolicyError: string;
  readonly id_termsLnk: string;
  readonly id_policyLnk: string;
  readonly id_signUpHint: string;
  readonly id_signUpError: string;
  readonly id_signUpPwdHint: string;

  constructor(page: Page) {
    super(page);
    this.id_Title = "div[class='ssp-step ssp-step-0 ssp-step-show'] > h1";
    this.id_SubTitle =
      "div[class='ssp-step ssp-step-0 ssp-step-show']> div[class='ab-signup-usa--free-text']";
    this.id_PlaceHolder = "label[class='signup__input-label']>>nth=0";
    this.id_CntBtnLbl = "button[data-testid='mr-form-signup-btn-start-1'] ";
    this.id_CntBtnEmail = "button[data-testid='mr-form-signup-btn-start-1']";
    this.id_CntBtnName = "button[data-testid='mr-form-signup-btn-start-2']";
    this.id_CntBtnPwd = "button[data-testid='mr-form-signup-btn-start-3']";
    this.id_EnterEmail = "input[id='email']";
    this.id_EnterName = "input[data-testid='mr-form-signup-name-1']";
    this.id_EnterPwd = "input[data-testid='mr-form-signup-password-1']";
    this.id_CheckBox = "label[class='mr-checkbox-1__check ']";
    this.id_CnfTitle = "h1[class='signup__title-form']";
    this.id_CnfSubTitle = "div[class='signup__subtitle-form']";
    this.id_CnfPlaceholderCode = "label[for='code']";
    this.id_CnfCode = "input[id='code']";
    this.id_CnfFooter = "div[class='signup__footer']";
    this.id_CnfSignUpSndCodeLnk = "div[class='signup__footer']>a>>nth=0";
    this.id_CnfSignUpHelpLnk = "div[class='signup__footer']>a>>nth=1";
    this.id_EmailError = "label[id='emailError']>>nth=0";
    this.id_invalidEmailError = "label[id='emailError']>>nth=1";
    this.id_nameError = "label[id='nameError']";
    this.id_pwdError = "div[data-testid='please-enter-your-password-1']";
    this.id_termsAndPolicyError = "label[id='termsError']";
    this.id_termsLnk = "a[data-testid='mr-link-terms-1']";
    this.id_policyLnk = "a[data-testid='mr-link-privacy-1']";
    this.id_signUpHint = "div[class='signup__input-hint-text']";
    this.id_signUpError = "label[class='js-error signup__error-item']>>nth=0";
    this.id_signUpPwdHint = "div[id='signup-form-password']>>nth=0";
  }

  async title(): Promise<string> {
    return (await this.getText(this.id_Title)).trim();
  }

  async subtitle(): Promise<string> {
    return (await this.getText(this.id_SubTitle)).trim();
  }

  async placeHolder(): Promise<string> {
    return (await this.getText(this.id_PlaceHolder)).trim();
  }

  async cntBtnLbl(): Promise<string> {
    return (await this.getText(this.id_CntBtnLbl)).trim();
  }

  async cntBtn(): Promise<number> {
    return await this.page.locator(this.id_CntBtnEmail).count();
  }

  async enterEmail(email: string): Promise<void> {
    await this.enterText(this.id_EnterEmail, email);
  }

  async cntBtnClick(fieldType: string): Promise<void> {
    switch (fieldType) {
      case 'NAME': {
        //click on Continue when on Email field;
        await this.btnClick(this.id_CntBtnName);
        break;
      }
      case 'PASSWORD': {
        //click on Continue when on Name field;
        await this.btnClick(this.id_CntBtnPwd);
        break;
      }
      case 'EMAIL': {
        //click on Continue when on Name field;
        await this.btnClick(this.id_CntBtnEmail);
        break;
      }
      default: {
        //click on Continue when on Name field;
        throw new Error(
          `Choose either ${SIGNUP_CONTINUE_BUTTON.name} or ${SIGNUP_CONTINUE_BUTTON.password} or ${SIGNUP_CONTINUE_BUTTON.email}`
        );
      }
    }
  }

  async enterName(uname: string = 'PlaywrightTestUser'): Promise<void> {
    await this.enterText(this.id_EnterName, uname);
  }

  async enterPwd(pwd: string = 'PlaywrightTest@123'): Promise<void> {
    await this.enterText(this.id_EnterPwd, pwd);
  }

  async checkBoxClick(): Promise<void> {
    await this.chkBxClick(this.id_CheckBox);
  }

  async cnfTitle(): Promise<string> {
    return (await this.getText(this.id_CnfTitle)).trim();
  }

  async cnfSubTitle(): Promise<string> {
    return (await this.getText(this.id_CnfSubTitle)).trim();
  }

  async cnfPlaceholderCode(): Promise<string> {
    return (await this.getText(this.id_CnfPlaceholderCode)).trim();
  }

  async cnfFooter(): Promise<string> {
    return (await this.getText(this.id_CnfFooter)).trim();
  }

  async cnfSndCodeLnk(): Promise<string | null> {
    return await this.page
      .locator(this.id_CnfSignUpSndCodeLnk)
      .getAttribute('href');
  }

  async cnfSndHelpLnk(): Promise<string | null> {
    return await this.page
      .locator(this.id_CnfSignUpHelpLnk)
      .getAttribute('href');
  }

  async emailError(): Promise<string> {
    return await this.getText(this.id_EmailError);
  }

  async invalidEmailError(): Promise<string> {
    return await this.getText(this.id_invalidEmailError);
  }

  async nameError(): Promise<string> {
    return await this.getText(this.id_nameError);
  }

  async pwdError(): Promise<string> {
    return await this.getText(this.id_pwdError);
  }

  async termsAndPolicyError(): Promise<string> {
    return await this.getText(this.id_termsAndPolicyError);
  }

  async termsLnk(): Promise<string | null> {
    return await this.page.locator(this.id_termsLnk).getAttribute('href');
  }

  async policyLnk(): Promise<string | null> {
    return await this.page.locator(this.id_policyLnk).getAttribute('href');
  }

  async signUpHint(): Promise<string> {
    return await this.getText(this.id_signUpHint);
  }

  async signUpError(): Promise<string> {
    return await this.getText(this.id_signUpError);
  }

  async signUpPwdHint(): Promise<string> {
    return await this.getText(this.id_signUpPwdHint);
  }
}
