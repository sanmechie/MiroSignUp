import { ElementHandle, Page, Response } from '@playwright/test';

export default class Base {
  /**
   *
   * This class exposes common methods required to interact
   * with all the pages across the AUT
   *
   */

  constructor(protected page: Page) {}

  async navigate(url: string) {
    await this.page.goto(url);
  }

  protected async getText(idenitfier: string): Promise<string> {
    let text;
    try {
      await this.page.locator(idenitfier).isVisible();
      text = await this.page.locator(idenitfier).textContent();
    } catch (e) {
      text = e.message;
    }
    return text.trim();
  }

  protected async btnClick(idenitfier: string): Promise<void> {
    await this.page.locator(idenitfier).isEnabled();
    await this.page.locator(idenitfier).click();
  }

  protected async enterText(idenitfier: string, text: string) {
    await this.page.locator(idenitfier).isEnabled();
    await this.page.locator(idenitfier).fill(text);
  }

  protected async chkBxClick(idenitfier: string): Promise<void> {
    await this.page.locator(idenitfier).isEnabled();
    await this.page.locator(idenitfier).click();
  }
}
