export enum SIGNUP_CONTINUE_BUTTON {
    name = 'NAME',
    password = 'PASSWORD',
    email = 'EMAIL'
  
  }

export const PASSWORDS  = {
  
   'WEAK PASSWORD': '12356789', 
   'SO-SO PASSWORD': 'SOSOPWDD',
   'GOOD PASSWORD': 'SOSOPWDD@123',
   'GREAT PASSWORD': 'GREATPass@123'
}

export const PASSWORD_HINTS  = {
  'LESS_THAN_8_PASS': ['1234567', 'Please use 8+ characters for secure password.'],
  'WEAK PASSWORD': ['12356789', 'Weak password'], 
  'SO-SO PASSWORD': ['SOSOPWDD', 'So-so password'],
  'GOOD PASSWORD':  ['PLyWriS1@', 'Good password'],
  'GREAT PASSWORD': ['GREATPass@19023', 'Great password'],
}