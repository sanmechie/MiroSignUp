const randomEmail = require('random-email');

export class Helper {
  async generateEmailID() {
    return await randomEmail({ domain: 'playwrighttest.com' });
  }
}
