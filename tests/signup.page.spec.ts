import test, { expect, Page, BrowserContext } from '@playwright/test';
import SignUp from './pages/SignUp';
import endpoint from './config/configTypes';
import { Helper } from './utilities/Helper';
import {
  SIGNUP_CONTINUE_BUTTON,
  PASSWORDS,
  PASSWORD_HINTS,
} from './utilities/Constants';
const SIGNUP_URL = endpoint.BASEURL + '/signup/';

test.describe('E2E Tests for Sign Up Functionality', async () => {
  let page: Page;
  let context: BrowserContext;
  let signUp: SignUp;
  let helper: Helper;

  test.beforeAll(async ({ browser }) => {
    context = await browser.newContext();
    page = await context.newPage();
    signUp = new SignUp(page);
    helper = new Helper();
  });

  test('Verify Sign Up Free today text @positive', async () => {
    await signUp.navigate(SIGNUP_URL);
    expect(await signUp.title()).toEqual('Sign up for free today');
  });

  test('Verify Sign Up recommendation text @positive', async () => {
    await signUp.navigate(SIGNUP_URL);
    expect(await signUp.subtitle()).toEqual(
      'We recommend using your work email — it keeps work and life separate.'
    );
  });

  test('Verify Sign Up placeholder text @positive', async () => {
    await signUp.navigate(SIGNUP_URL);
    expect(await signUp.placeHolder()).toEqual('Work email');
  });

  test('Verify Sign Up button text @positive', async () => {
    await signUp.navigate(SIGNUP_URL);
    const text = `Continue
   →`;
    expect(await signUp.cntBtnLbl()).toEqual(text);
  });

  test('Verify Sign Up button exists @positive', async () => {
    await signUp.navigate(SIGNUP_URL);
    expect(await signUp.cntBtn()).toBe(1);
  });

  for (const pass in PASSWORDS) {
    test(`Verify successfully registration message with ${pass} @positive`, async () => {
      await signUp.navigate(SIGNUP_URL);
      const email = await helper.generateEmailID();
      const msg = `We've sent you a six-digit confirmation code to ${email}. Please enter it below to confirm your email address.`;
      await signUp.enterEmail(email);
      await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.email);
      await signUp.enterName();
      await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.name);
      await signUp.enterPwd(await PASSWORDS[pass]);
      // policy link should exist
      expect(await signUp.policyLnk()).not.toBeNull();
      // terms links should exist
      expect(await signUp.termsLnk()).not.toBeNull();
      await signUp.checkBoxClick();
      await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.password);
      expect(await signUp.cnfTitle()).toEqual('Check your email');
      expect(await signUp.cnfSubTitle()).toEqual(msg);
      expect(await signUp.cnfPlaceholderCode()).toEqual('Enter 6-digit code');
      expect(await signUp.cnfFooter()).toEqual(
        'Send code again or find more information in Help Center.'
      );
      // send code link should exist
      expect(await signUp.cnfSndCodeLnk()).not.toBeNull();
      // help link should exist
      expect(await signUp.cnfSndHelpLnk()).not.toBeNull();
    });
  }

  for (const [pass, expected_val] of Object.entries(PASSWORD_HINTS)) {
    test(`Verify user hint for ${pass} @positive`, async () => {
      await signUp.navigate(SIGNUP_URL);
      const email = await helper.generateEmailID();
      await signUp.enterEmail(email);
      await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.email);
      await signUp.enterName();
      await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.name);
      await signUp.enterPwd(expected_val[0]);
      // This is not good hardcode wait, find a better wauy
      await page.waitForTimeout(1000);
      expect(await signUp.signUpPwdHint()).toEqual(expected_val[1]);
    });
  }

  test(`Verify user cannot register when password is less than 8 characters @positive`, async () => {
    // This is a slightly misleading behavior, hint says 8+ for secure password
    // but still allows user to succesfully register with 8 characters
    // So I will fail this and test can be updated post confirmation with PO.
    await signUp.navigate(SIGNUP_URL);
    const email = await helper.generateEmailID();
    await signUp.enterEmail(email);
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.email);
    await signUp.enterName();
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.name);
    await signUp.enterPwd('12345678');
    await signUp.checkBoxClick();
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.password);
    await expect(page).toHaveURL('https://miro.com/signup/');
    expect(await signUp.signUpHint()).toEqual(
      'Please use 8+ characters for secure password.'
    );
  });

  test(`Verify duplicate email ID cannot be registered @positive`, async () => {
    await signUp.navigate(SIGNUP_URL);
    const email = await helper.generateEmailID();
    await signUp.enterEmail(email);
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.email);
    await signUp.enterName();
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.name);
    await signUp.enterPwd();
    await signUp.checkBoxClick();
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.password);
    await signUp.navigate(SIGNUP_URL);
    await signUp.enterEmail(email);
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.email);
    await signUp.enterName();
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.name);
    await signUp.enterPwd();
    await signUp.checkBoxClick();
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.password);
    expect(await signUp.signUpError()).toEqual(
      'Sorry, this email is already registered'
    );
    await expect(page).toHaveURL('https://miro.com/signup/');
  });

  test('Verify successfully registration with a weak password @positive', async () => {
    await signUp.navigate(SIGNUP_URL);
    const email = await helper.generateEmailID();
    await signUp.enterEmail(email);
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.email);
    await signUp.enterName();
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.name);
    await signUp.enterPwd('12345678');
    await signUp.checkBoxClick();
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.password);
    // this should confirm that user has succesfully registered
    await expect(page).toHaveURL('https://miro.com/email-confirm/');
  });

  test('Verify Error is shown when No Email is entered @negative', async () => {
    await signUp.navigate(SIGNUP_URL);
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.email);
    expect(await signUp.emailError()).toEqual('Enter your email address.');
  });

  test('Verify Error is shown when invalid Email is entered @negative', async () => {
    await signUp.navigate(SIGNUP_URL);
    await signUp.enterEmail('DUMMYEMAIL');
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.email);
    await Promise.all([
      await page.waitForResponse(
        (resp) =>
          resp.url().includes('/api/v1/email/validation') &&
          resp.status() === 400
      ),
      expect(await signUp.invalidEmailError()).toEqual(
        'Enter a valid email address.'
      ),
    ]);
  });

  test('Verify Error is shown when No Name is entered @negative', async () => {
    await signUp.navigate(SIGNUP_URL);
    await signUp.enterEmail(await helper.generateEmailID());
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.email);
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.name);
    expect(await signUp.nameError()).toEqual('Please enter your name.');
  });

  test('Verify Error is shown when No Password is entered and Terms and Policy is not checked @negative', async () => {
    await signUp.navigate(SIGNUP_URL);
    await signUp.enterEmail(await helper.generateEmailID());
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.email);
    await signUp.enterName();
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.name);
    await signUp.cntBtnClick(SIGNUP_CONTINUE_BUTTON.password);
    expect(await signUp.termsAndPolicyError()).toEqual(
      'Please agree with the Terms to sign up.'
    );

    expect(await signUp.pwdError()).toEqual('Enter your password.');
  });
});
