# Project description
```
This is e2e tests for Sign up functionality of Miro using [Playwright](https://playwright.dev/) .
By default all tests are executed in headless mode and all 3 browsers Chromium, Firefox and Safari.
All positive workflow are annotated with @positive tag while negative workflow are annotated with @negative
Failed test cases will be automatically retired 2 times. You can control this setting from playwright.config.ts
Author: chaugule.sandeep@gmail.com
```

## Node and NPM version
```
Tested in below versions of Node and NPM
node version == v16.16.0
npm version == 8.11.0
```

## Setting up env
```
npm install ci
Recommened IDE: VS Code
```

## Running tests
```
npm test
```

## Running in headed mode
```
npx playwright test --headed
```

## Running in parallel
```
By default it will use 2 workers to run tests in parallel, You can change that by passing
npx playwright test --workers ${number}
Ex: npx playwright test --workers 3
```

## Running in specific browser
```
npx playwright test --project ${browsertype}
Ex: npx playwright test --project chromium
```

## Running using annotations
```
npx playwright test --grep ${tag}
Ex: npx playwright test --grep @positive
```

## To serve reports
```
By default it will use 2 workers to run tests in parallel, You can change that by passing
npx playwright test --workers ${number}
Ex: npx playwright test --workers 3
```

## Improvements
```
1. Remove hardcoded waits
2. Improve error handling
3. Better Reporting
```
